import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActividadesAddComponent } from './Actividades/add-actividades/actividades-add.component';
import { ActividadesEditComponent } from './Actividades/edit-actividades/actividades-edit.component';
import { ActividadesGetComponent } from './Actividades/get-actividades/actividades-get.component';
import { ResponsableAddComponent } from './Responsable/add-responsable/responsable-add.component';
import { ResponsableEditComponent } from './Responsable/edit-responsable/responsable-edit.component';
import { ResponsableGetComponent } from './Responsable/get-responsable/responsable-get.component';

const routes: Routes = [
  {
  path: 'actividades/create',
  component: ActividadesAddComponent
},
{
  path: 'actividades/edit/:id',
  component: ActividadesEditComponent
},
{
  path: 'actividades',
  component: ActividadesGetComponent
},
{
  path: 'responsable/create',
  component: ResponsableAddComponent
},
{
  path: 'responsable/edit/:id',
  component: ResponsableEditComponent
},
{
  path: 'responsable',
  component: ResponsableGetComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
