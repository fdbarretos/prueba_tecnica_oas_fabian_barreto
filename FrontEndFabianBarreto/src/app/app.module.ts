import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppComponent } from './app.component';
import { ActividadesAddComponent } from './Actividades/add-actividades/actividades-add.component';
import { ActividadesEditComponent } from './Actividades/edit-actividades/actividades-edit.component';
import { ActividadesGetComponent } from './Actividades/get-actividades/actividades-get.component';
import { ResponsableAddComponent } from './Responsable/add-responsable/responsable-add.component';
import { ResponsableEditComponent } from './Responsable/edit-responsable/responsable-edit.component';
import { ResponsableGetComponent } from './Responsable/get-responsable/responsable-get.component';

import { ToastrModule } from 'ng6-toastr-notifications';
import { ActividadesService } from './services/actividades.service';
import { ResponsableService } from './services/responsable.service';


@NgModule({
  declarations: [
    AppComponent,
ActividadesAddComponent,
ActividadesGetComponent,
ActividadesEditComponent
,
ResponsableAddComponent,
ResponsableGetComponent,
ResponsableEditComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    ToastrModule.forRoot()
  ],
  providers: [ 
    ActividadesService,
ResponsableService
 
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
