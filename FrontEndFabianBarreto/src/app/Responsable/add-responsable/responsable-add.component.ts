import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { ResponsableService } from '../../services/responsable.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-responsable-add',
  templateUrl: './responsable-add.component.html',
  styleUrls: ['./responsable-add.component.css']
})
export class ResponsableAddComponent implements OnInit {
   
  angForm: FormGroup;
  constructor(private fb: FormBuilder, private Responsable_ser: ResponsableService, public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombres: ['', Validators.required ],
apellidos: ['', Validators.required ],
email: ['', Validators.required ],
telefono: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addResponsable(nombres, apellidos, email, telefono ) {
    this.Responsable_ser.addResponsable(nombres, apellidos, email, telefono );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    
  }

}
