import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActividadesService {

  uri = 'http://localhost:2020/actividades';

  constructor(private http: HttpClient) { }

  addActividades(fecha_creacion ,fecha_limite ,responsable ,descripcion ,estado ) {
    const obj = {
      fecha_creacion:fecha_creacion,
fecha_limite:fecha_limite,
responsable:responsable,
descripcion:descripcion,
estado:estado

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }

  getActividades() {
    return this
           .http
           .get(`${this.uri}`);
  }

  editActividades(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }

  updateActividades(fecha_creacion ,fecha_limite ,responsable ,descripcion ,estado , id) {

    const obj = {
      fecha_creacion:fecha_creacion,
fecha_limite:fecha_limite,
responsable:responsable,
descripcion:descripcion,
estado:estado

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }

 deleteActividades(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
