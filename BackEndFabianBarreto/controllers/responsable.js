//importar modelo
var Responsable = require('../models/responsable');

//funcion create
exports.responsable_create = function (req, res) {
    var responsable = new Responsable(
        req.body
    );

    responsable.save(function (err, responsable) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: responsable._id})
    })
};
//funcion read by id
exports.responsable_details = function (req, res) {
    Responsable.findById(req.params.id, function (err, responsable) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(responsable);
    })
};
//funcion read all
exports.responsable_all = function (req, res) {
    Responsable.find(req.params.id, function (err, responsable) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(responsable);
    })
};
//funcion update
exports.responsable_update = function (req, res) {
    Responsable.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, responsable) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", responsable: responsable });
    });
};

//funcion delete
exports.responsable_delete = function (req, res) {
    Responsable.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};