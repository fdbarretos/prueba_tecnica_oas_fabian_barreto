// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de actividades
var actividades_controller = require('../controllers/actividades');

// GET /:id
router.get('/:id', actividades_controller.actividades_details);
// GET /
router.get('/', actividades_controller.actividades_all);
// POST /
router.post('/', actividades_controller.actividades_create);
// PUT /:id
router.put('/:id', actividades_controller.actividades_update);
// DELETE /:id
router.delete('/:id', actividades_controller.actividades_delete);
// PUT responsable 
router.put('/responsable/:id',actividades_controller.Actividades_add_responsable);

//export router
module.exports = router;