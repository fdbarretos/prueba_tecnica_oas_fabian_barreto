// importar biblioteca express
var express = require('express');
// importar biblioteca body-parser
var bodyParser = require('body-parser');
// importar biblioteca morgan
var logger = require('morgan')
// importar router actividades
var actividades = require('./routes/actividades');
// importar router responsable
var responsable = require('./routes/responsable');

var cors = require('cors');
// importar router de status
var status = require('./routes/status'); 
// uso biblioteca express
var app = express();
//visualizacion respuestas en consola
app.use(logger('dev'))
//agrega CORS para tener accesso desde otras aplicaciones
app.use(cors());
// uso biblioteca mongoose
var mongoose = require('mongoose');
// conexion DB 
var dev_db_url = 'mongodb://root:example@mongodboasfb:27017/PruebaOAS_Fabian_Barreto?authSource=admin';
mongoose.connect(dev_db_url);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// uso de respuestas y llamados en JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//creacion ruta /actividades
app.use('/actividades', actividades);
//creacion ruta /responsable
app.use('/responsable', responsable);

//creacion ruta /
app.use('/', status);
// puerto de servicio
var port = 2020;
// inicir la conexion
app.listen(port, () => {
    console.log('Server in port ' + port);
});