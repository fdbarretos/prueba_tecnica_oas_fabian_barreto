# Prueba Tecnica Oficina Asesora de Sistemas - UD - Universidad Distrital Francisco Jose de Caldas

_Desarrollo de un entorno dockerizado para el manejo de dos entidades con separación de frontend y backend un proyecto que maneje dos entidades:_

_Actividades:_
* Identificador
* Fecha de creación
* Fecha límite
* Responsable
* Descripción
* Estado

_Responsable:_
* Identificador
* Nombres
* Apellidos
* email
* Teléfono

_Se pide un reporte de actividades por estado y un reporte de actividades por responsable, la entrega debe realizarse mediante un repositorio GIT._



## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.

```
git clone https://gitlab.com/fdbarretos/prueba_tecnica_oas_fabian_barreto
```

### Pre-requisitos 📋

_Debe contar con los siguiente requisitos:_

* Tener instalado Portainer.io en su sistema operativo.
* Los Puertos **2020** (BackEnd) y **4200**(FrontEnd) deben estar libres para ejecutar el desarrollo.


## Despliegue de los contededores  ⚙️

_Una ves clonado el repositorio de forma local se debe realizar el despliegue de los contenedores con los siguientes pasos:_

* Estar en el Directorio principal del repositorio desde la consola de su sistema operativo.
* Realizar el despliegue de los contenedores (mongoprueba_oas_fabian_barreto, backprueba_oas_fabian_barreto y frontprueba_oas_fabian_barreto) con el siguiente comando:

```
docker-compose up
```
* Una ves terminado el despliegue de contenedores, por favor parar los servicios desde consola (Ctrl+c) y volver a desplegar los servicios con el comando docker-compose up, esto es debedido a que el servicio de mongo desplegado requiere una recarga para el correcto funcionamiento.  

### Analice las pruebas end-to-end 🔩

_Realizamos la pruebas del desarrollo de forma local, para ingresar al FronEnd, ingresamos en el navegador a la dirección_

```
localhost:4200

```

_Cree primero un resposable llenando la información requerida, despues de crear el responsable podra asignar una actidad a cualquiera de los responsable creados_

_En las pestañas Actividades y resposable, podra observar el reporte de cada una de las entidades_

_Si desea revisar el estado  del BackEnd del desarrollo, tiene que ingresar a la dirección_

```
localhost:2020
```
_Si desea revisar la base de datos en formato .js podra acerlo con las direcciones_

```
localhost:2020/actividades
```

```
localhost:2020/responsables
```


## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Mongo](https://www.mongodb.com/) - Gestor de base de datos
* [Angular](https://angular.io/) - Framework del FrontEnd
* [Portainer](hhttps://www.portainer.io/) - Gestor de contenedores


## Autores ✒️

_Desarrollo realizado por:_

* **Fabian David Barreto Sanchez** - *Desarrollador Principal* - [fdbarretos](https://gitlab.com/fdbarretos)



